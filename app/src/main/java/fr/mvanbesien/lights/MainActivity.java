package fr.mvanbesien.lights;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    private final int[] images = new int[]{R.drawable.off, R.drawable.blue, R.drawable.green, R.drawable.purple, R.drawable.red, R.drawable.yellow, R.drawable.orange};

    private final Random r = new Random();

    private boolean doubleBackToExitPressedOnce;

    private final Handler mHandler = new Handler();

    private SharedPreferences sharedPreferences;

    private Map<Integer, Timer> autochangeTimers = new HashMap<>();

    private void stopTimer(int resourceId) {
        Timer timer = autochangeTimers.get(resourceId);
        if (timer != null) {
            timer.cancel();
            autochangeTimers.remove(resourceId);
        }
    }

    private void startTimer(final int resourceId) {
        String timerName = "LightsTimer-"+Long.toString(System.currentTimeMillis(), Character.MAX_RADIX);
        Timer timer = new Timer(timerName, true);
        long pace = r.nextInt(1700) + 300;
        autochangeTimers.put(resourceId, timer);
        timer.scheduleAtFixedRate(new TimerTask() {

            private final Random r = new Random();

            private ImageView view = ((ImageView) MainActivity.this.findViewById(resourceId));

            @Override
            public void run() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int nextInt = r.nextInt(images.length);
                        int image = images[nextInt];
                        view.setImageResource(image);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(String.valueOf(view.getId()), nextInt);
                        editor.commit();
                    }
                });
            }
        }, 0, pace);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        this.sharedPreferences = getSharedPreferences("fr.mvanbesien.lights.MainActivity", 0);

        activateImageView(R.id.imageView);
        activateImageView(R.id.imageView2);
        activateImageView(R.id.imageView3);
        activateImageView(R.id.imageView4);
        activateImageView(R.id.imageView5);
        activateImageView(R.id.imageView6);
    }

    private void activateImageView(int resourceId) {
        View v = findViewById(resourceId);
        if (v instanceof ImageView) {
            int savedValue = this.sharedPreferences.getInt(String.valueOf(resourceId), R.drawable.off);
            ((ImageView) v).setImageResource(savedValue >= 0 && savedValue < images.length ? images[savedValue] : R.drawable.off);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stopTimer(view.getId());
                    int nextInt = r.nextInt(images.length);
                    int image = images[nextInt];
                    ((ImageView) view).setImageResource(image);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt(String.valueOf(view.getId()), nextInt);
                    editor.commit();
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        startTimer(view.getId());
                        return true;
                    }
                }
            );
        }
    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
        for (Timer timer : autochangeTimers.values()) {
            timer.cancel();
        }
    }
}
